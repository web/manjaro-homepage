+++
class= "hardware"
model = "SER4"
price = "From 599 USD"
brand = "Beelink"
intro = ""
type = "hardware"
image = "SER4"
buylink = "https://www.bee-link.com/catalog/product/buy?id=289"
specs = "AMD Ryzen7 4800U Octa Core, AMD Radeon Graphics, up to 32GB DDR4 RAM 3200MHz, 500GB M.2 2280 NVMe SSD, HDMI*2 USB-C Triple Output"
+++
