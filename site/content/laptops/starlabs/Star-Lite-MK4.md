+++
class= "hardware"
model = "Star Lite MK4"
price = "From 400 GBP"
brand = "Starlabs"
type = "hardware"
image = "StarLite-Manjaro-03x1500"
buylink = "https://starlabs.systems/pages/starlite?partner=manjaro"
specs = "Intel® Pentium® N5030 with Intel® UHD Graphics 605, 8 GB 2400MHz, 240GB SSD, Display 11.6\" matte display with IPS technology, Battery Up to 8H"
+++

