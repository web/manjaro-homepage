+++
class= "hardware"
model = "StarBook MK5"
price = "From 778 GBP"
brand = "Starlabs"
type = "hardware"
image = "StarBook-2-Manjaro-03x1500"
buylink = "https://starlabs.systems/pages/starbook?partner=manjaro"
specs = "Intel® Core® i3-1110G4 (2.4GHz dual-core) or i7-1165G7 (2.8GHz quad-core) with Intel® UHD G4 Graphics, 14\" IPS LED-backlit matte display, 8-64GB 3200MHz, 240-1000GB SSD, Battery Up to 11H"
+++
