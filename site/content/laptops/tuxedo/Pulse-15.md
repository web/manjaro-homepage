+++
class= "hardware"
model = "Pulse 15 Gen1"
price = "From 1085 EUR"
brand = "Tuxedo"
intro = "TUXEDO Computers are individually built, assembled and installed in Germany and delivered in such a way that you only have to unpack, connect and switch them on!"
type = "hardware"
image = "15-Pulse"
buylink = "https://www.tuxedocomputers.com/en/Linux-Hardware/Linux-Notebooks/15-16-inch/Manjaro-Pulse-15-Gen1.tuxedo"
specs = "AMD Ryzen™ 7 4800H with Radeon™ RX Vega 7, DDR4 3200 MHz 8 to 64 GB, 250GB to 2000GB, 15,6 Full-HD IPS non-glare, Manjaro super-key and housing, Battery up to 20H"
+++

