+++
class= "hardware"
model = "Aura 15 Gen1"
price = "From 904 EUR"
brand = "Tuxedo"
intro = "TUXEDO Computers are individually built, assembled and installed in Germany and delivered in such a way that you only have to unpack, connect and switch them on!"
type = "hardware"
image = "Aura-15"
buylink = "https://www.tuxedocomputers.com/en/Linux-Hardware/Linux-Notebooks/Manjaro/Manjaro-Aura-15-Gen1.tuxedo"
specs = "AMD Ryzen™ 3 4300U / Ryzen 5 4500U / Ryzen™ 7 4700U, AMD Radeon™ RX Vega, DDR4 3200 MHz 8 GB to 64 GB, 250GB to 2000GB, 14 Full-HD IPS-Panel non-glare, optional LTE/HSDPA+ 4G HUAWEI ME936, Manjaro Super-key and housing, Battery up to 15h"
+++
