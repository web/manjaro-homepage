+++
class= "hardware"
model = "Manjaro Mini Pc I3-7"
price = "From 555 EUR"
brand = "Manjaro"
type = "hardware"
image = "manjarobox"
buylink = "https://manjarocomputer.eu/index.php/manjaro-mini-pc-i3-7.html"
specs = "Intel® Core™ i3-10110U, 4GB DDR4 SODIMM CL17 (2400MHz), Intel® Iris Plus Graphics 655, 120GB SSD, Intel® Wireless-AC 8265 & BT"
+++
