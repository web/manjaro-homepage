+++
class= "hardware"
model = "ManjaroBook 15.6\" Alloy AMD"
price = "From 1250 EUR"
brand = "Manjaro"
type = "hardware"
image = "ManjaroBook_15-6_Alloy_AMD"
buylink = "https://manjarocomputer.eu/index.php/manjarobook-15-6-alloy-amd-cpu.html"
specs = "Metal Design, Magnesium Alloy Chassis, AMD Ryzen™ 7 5700U, AMD® Radeon™, 8GB DDR4, 512GB SSD, Gigabit LAN & Wireless Intel® Wi-Fi 6 AX200 (2.4 Gbps) + BT 5.0"
+++

