+++
Download_x64 = "https://download.manjaro.org/mate/21.2.6/manjaro-mate-21.2.6-220509-linux515.iso"
Download_x64_Checksum = "09ad1a771d9d882a30c7398aaef32545343175ab"
Download_x64_Sig = "https://download.manjaro.org/mate/21.2.6/manjaro-mate-21.2.6-220509-linux515.iso.sig"
Download_x64_Torrent = "https://download.manjaro.org/mate/21.2.6/manjaro-mate-21.2.6-220509-linux515.iso.torrent"
Download_Minimal_x64 = "https://download.manjaro.org/mate/21.2.6/manjaro-mate-21.2.6-minimal-220509-linux515.iso"
Download_Minimal_x64_Checksum = "0c8f1c388bc5895912e4bfc517b11c4ad01b66ee"
Download_Minimal_x64_Sig = "https://download.manjaro.org/mate/21.2.6/manjaro-mate-21.2.6-minimal-220509-linux515.iso.sig"
Download_Minimal_x64_Torrent = "https://download.manjaro.org/mate/21.2.6/manjaro-mate-21.2.6-minimal-220509-linux515.iso.torrent"
Name = "Mate"
Screenshot = "mate-full.jpg"
edition = "Community"
shortDescription = "For people who look for a traditional experience"
Thumbnail = "mate-full.jpg"
Version = "21.2.6"
date = "2022-05-09T19:23:00UTC"
title = "Mate"
type="download-edition"
weight = "5"
meta_description = "Manjaro mate for people who look for a traditional experience"
meta_keywords = "manjaro mate download, manjaro download"
+++

This edition is supported by the Manjaro community and comes with Mate, a desktop environment that continues the legacy of traditional user experience while carefully improving and modernizing it when needed.

If you are looking for older images check the [Mate](https://osdn.net/projects/manjaro-archive/storage/mate/) archive.

