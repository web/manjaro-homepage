+++
Download_x64 = "https://download.manjaro.org/lxqt-kwin/21.2.6/manjaro-lxqt-kwin-21.2.6-220509-linux515.iso"
Download_x64_Checksum = "10a357f317d4e9f5f294eb5bb02ffaa7e20dc60d"
Download_x64_Sig = "https://download.manjaro.org/lxqt-kwin/21.2.6/manjaro-lxqt-kwin-21.2.6-220509-linux515.iso.sig"
Download_x64_Torrent = "https://download.manjaro.org/lxqt-kwin/21.2.6/manjaro-lxqt-kwin-21.2.6-220509-linux515.iso.torrent"

Name = "Lxqt-Kwin"
Screenshot = "lxqt-kwin-full.jpg"
shortDescription = "Lightweight desktop build with the LXQt toolkit and the Kwin Window Manager."
edition = "Community"
Thumbnail = "lxqt-kwin-full.jpg"
Version = "21.2.6"
date = "2022-05-09T14:40:43UTC"
title = "Lxqt-Kwin"
type="download-edition"
weight = "5"
+++

Manjaro LXQt provides a lightweight LXQt desktop environment with a basic set of applications.
This build uses the Kwin Window Manager.

If you are looking for older images check the [lxqt-kwin](https://osdn.net/projects/manjaro-archive/storage/lxqt-kwin/) archive.
