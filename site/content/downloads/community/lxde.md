+++
Download_x64 = "https://download.manjaro.org/lxde/21.2.6/manjaro-lxde-21.2.6-220509-linux515.iso"
Download_x64_Checksum = "aeb062b52c32179a5a5a0f0fc3fb1e97b8f1bb9f"
Download_x64_Sig = "https://download.manjaro.org/lxde/21.2.6/manjaro-lxde-21.2.6-220509-linux515.iso.sig"
Download_x64_Torrent = "https://download.manjaro.org/lxde/21.2.6/manjaro-lxde-21.2.6-220509-linux515.iso.torrent"

Name = "Lxde"
Screenshot = "lxde-full.png"
shortDescription = "Lightweight desktop build using the GTK toolkit"
edition = "Community"
Thumbnail = "lxde-full.jpg"
Version = "21.2.6"
date = "2022-05-09T14:41:53UTC"
title = "Lxde"
type="download-edition"
weight = "5"
+++

Manjaro LXDE provides a lightweight GTK desktop environment. In addition to LXDE itself it comes with Manjaro Hello and the integrated Application utility, providing quick access to popular applications. Additionally the Kvantum theme manager is provided to provide consistent theming for both QT and GTK applications.

This edition is supported by the Manjaro community and uses Openbox Window Manager.

If you are looking for older images check the [LXDE](https://osdn.net/projects/manjaro-archive/storage/lxde/) archive.
