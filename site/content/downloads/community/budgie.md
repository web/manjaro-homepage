+++
Download_x64 = "https://download.manjaro.org/budgie/21.2.6/manjaro-budgie-21.2.6-220509-linux515.iso"
Download_x64_Checksum = "646ee96cec0b57984cee78dcaccc1fcdd5829e43"
Download_x64_Sig = "https://download.manjaro.org/budgie/21.2.6/manjaro-budgie-21.2.6-220509-linux515.iso.sig"
Download_x64_Torrent = "https://download.manjaro.org/budgie/21.2.6/manjaro-budgie-21.2.6-220509-linux515.iso.torrent"
Download_Minimal_x64 = "https://download.manjaro.org/budgie/21.2.6/manjaro-budgie-21.2.6-minimal-220509-linux515.iso"
Download_Minimal_x64_Checksum = "e5e7567be23972c5554adbcf4d3ca99b0b8f19e3"
Download_Minimal_x64_Sig = "https://download.manjaro.org/budgie/21.2.6/manjaro-budgie-21.2.6-minimal-220509-linux515.iso.sig"
Download_Minimal_x64_Torrent = "https://download.manjaro.org/budgie/21.2.6/manjaro-budgie-21.2.6-minimal-220509-linux515.iso.torrent"
Name = "Budgie"
Screenshot = "budgie-full.jpg"
edition = "Community"
shortDescription = "For people who want a simple and elegant desktop"
Thumbnail = "budgie-full.jpg"
Version = "21.2.6"
date = "2022-05-09T19:30:14UTC"
title = "Budgie"
type="download-edition"
weight = "5"
meta_description = "Manjaro budgie for people who want a simple and elegant desktop"
meta_keywords = "manjaro budgie download, manjaro download"
+++

This edition is supported by the Manjaro community and comes with Budgie, the desktop of the [Solus project](https://getsol.us/). Budgie focuses on providing a simple-to-use and elegant desktop that fulfills the needs of a modern user.

If you are looking for older images check the [Budgie](https://osdn.net/projects/manjaro-archive/storage/budgie/) archive.


