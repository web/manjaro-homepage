+++
Download_x64 = "https://download.manjaro.org/lxqt/21.2.6/manjaro-lxqt-21.2.6-220509-linux515.iso"
Download_x64_Checksum = "10383e5dbadc9229b3ebbc09b9dbffdf08380c48"
Download_x64_Sig = "https://download.manjaro.org/lxqt/21.2.6/manjaro-lxqt-21.2.6-220509-linux515.iso.sig"
Download_x64_Torrent = "https://download.manjaro.org/lxqt/21.2.6/manjaro-lxqt-21.2.6-220509-linux515.iso.torrent"

Name = "Lxqt"
Screenshot = "lxqt-full.png"
shortDescription = "Lightweight desktop build with the LXQt toolkit."
edition = "Community"
Thumbnail = "lxqt-full.jpg"
Version = "21.2.6"
date = "2022-05-09T14:40:43UTC"
title = "Lxqt"
type="download-edition"
weight = "5"
+++

Manjaro LXQt provides a lightweight LXQt desktop environment with a basic set of applications.
This build uses the Openbox Window Manager.

If you are looking for older images check the [lxqt](https://osdn.net/projects/manjaro-archive/storage/lxqt/) archive.
