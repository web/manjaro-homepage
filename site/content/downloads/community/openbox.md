+++
Download_x64 = "https://download.manjaro.org/openbox/21.2.6/manjaro-openbox-21.2.6-220509-linux515.iso"
Download_x64_Checksum = "b33222d2df545a4a28852475f8feecc9063b9206"
Download_x64_Sig = "https://download.manjaro.org/openbox/21.2.6/manjaro-openbox-21.2.6-220509-linux515.iso.sig"
Download_x64_Torrent = "https://download.manjaro.org/openbox/21.2.6/manjaro-openbox-21.2.6-220509-linux515.iso.torrent"

Name = "Openbox"
Screenshot = "openbox-full.png"
edition = "Community"
shortDescription = "For power users, developers, music production. It is famous for its endless configuration possibilties. A stacking window manager for extensive keyboard based workflows as well as mouse support."
Thumbnail = "openbox-full.jpg"
Version = "21.2.6"
date = "2022-05-09T14:39:49UTC"
title = "Openbox"
type="download-edition"
weight = "5"
meta_description = "Manjaro openbox for power users, developers, music production."
meta_keywords = "manjaro openbox download, manjaro download"
+++

This edition is supported by the Manjaro community and comes with openbox, a highly configurable window manager with extensive standards support and well known for stability.

It comes with Manjaro Application Manager for easy installation of popular applications and of course Pamac which is the Manjaro distributions in-house Package Management application. The Kvantum theme manager helps creating consistent theming for GTK and Qt applications alike.

If you are looking for older images check the [Openbox](https://osdn.net/projects/manjaro-archive/storage/openbox/) archive.
