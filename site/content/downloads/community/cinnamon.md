+++
Download_x64 = "https://download.manjaro.org/cinnamon/21.2.6/manjaro-cinnamon-21.2.6-220509-linux515.iso"
Download_x64_Checksum = "540b8ceec998bde4a8ec189dad1d344bfe6775bc"
Download_x64_Sig = "https://download.manjaro.org/cinnamon/21.2.6/manjaro-cinnamon-21.2.6-220509-linux515.iso.sig"
Download_x64_Torrent = "https://download.manjaro.org/cinnamon/21.2.6/manjaro-cinnamon-21.2.6-220509-linux515.iso.torrent"
Download_Minimal_x64 = "https://download.manjaro.org/cinnamon/21.2.6/manjaro-cinnamon-21.2.6-minimal-220509-linux515.iso"
Download_Minimal_x64_Checksum = "d788d7d3e159ba4b7a0b35952f0a8a40ab8ab703"
Download_Minimal_x64_Sig = "https://download.manjaro.org/cinnamon/21.2.6/manjaro-cinnamon-21.2.6-minimal-220509-linux515.iso.sig"
Download_Minimal_x64_Torrent = "https://download.manjaro.org/cinnamon/21.2.6/manjaro-cinnamon-21.2.6-minimal-220509-linux515.iso.torrent"
Name = "Cinnamon"
Screenshot = "cinnamon-full.jpg"
edition = "Community"
shortDescription = "For people who look for a traditional desktop with modern technology"
Thumbnail = "cinnamon-full.jpg"
Version = "21.2.6"
date = "2022-05-09T12:56:36UTC"
title = "Cinnamon"
type="download-edition"
weight = "5"
meta_description = "Manjaro cinnamon for people who look for a traditional desktop with modern technology"
meta_keywords = "manjaro cinnamon download, manjaro download"
+++

This edition is supported by the Manjaro community and comes with Cinnamon, a desktop based on modern technology that keeps known and proven concepts.

If you are looking for older images check the [Cinnamon](https://osdn.net/projects/manjaro-archive/storage/cinnamon/) archive.
