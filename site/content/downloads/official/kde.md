+++
Download_x64 = "https://download.manjaro.org/kde/21.2.6/manjaro-kde-21.2.6-220416-linux515.iso"
Download_x64_Checksum = "9aba53bb75e9a2e247734ff0ff601699eeef87f7"
Download_x64_Sig = "https://download.manjaro.org/kde/21.2.6/manjaro-kde-21.2.6-220416-linux515.iso.sig"
Download_x64_Torrent = "https://download.manjaro.org/kde/21.2.6/manjaro-kde-21.2.6-220416-linux515.iso.torrent"

Download_Minimal_x64 = "https://download.manjaro.org/kde/21.2.6/manjaro-kde-21.2.6-minimal-220416-linux515.iso"
Download_Minimal_x64_Checksum = "a4b8080266f16295ffa51ac9d196c6b8e7b9f47e"
Download_Minimal_x64_Sig = "https://download.manjaro.org/kde/21.2.6/manjaro-kde-21.2.6-minimal-220416-linux515.iso.sig"
Download_Minimal_x64_Torrent = "https://download.manjaro.org/kde/21.2.6/manjaro-kde-21.2.6-minimal-220416-linux515.iso.torrent"

Download_Minimal_lts = "https://download.manjaro.org/kde/21.2.6/manjaro-kde-21.2.6-minimal-220416-linux510.iso"
Download_Minimal_x64_Checksum_lts = "3a5d4cf813092f5be084aa0a2acbbaf72c7e63ee"
Download_Minimal_x64_Sig_lts = "https://download.manjaro.org/kde/21.2.6/manjaro-kde-21.2.6-minimal-220416-linux510.iso.sig"
Download_Minimal_x64_Torrent_lts = "https://download.manjaro.org/kde/21.2.6/manjaro-kde-21.2.6-minimal-220416-linux510.iso.torrent"

Name = "KDE Plasma"
Screenshot = "kde-full.jpg"
Youtube = "Mr9TkovpaME"
edition = "Official"
shortDescription = "Built-in interface to easily access and install themes, widgets, etc. While very user-friendly and certainly flashy."
Thumbnail = "kde.jpg"
Version = "21.2.6"
date = "2022-04-16T05:38:06UTC"
title = "KDE Plasma"
type="download-edition"
weight = "2"
meta_description = "Manjaro kde built-in interface to easily access and install themes, widgets, etc. While very user-friendly and certainly flashy."
meta_keywords = "manjaro kde download, manjaro download"
+++

This edition is supported by the Manjaro team and comes with KDE Plasma, a very modern and flexible desktop.

KDE Plasma is for people who want a user-friendly and customizable desktop. It is a feature-rich and versatile desktop environment that provides several different styles of menu to access applications. An excellent built-in interface to easily access and install new themes, widgets, etc, from the internet is also worth mentioning. KDE Plasma is simple by default, a clean work area for real-world usage which intends to stay out of your way, so the user is enabled to create the workflow that makes it more effective to complete tasks.

If you are looking for older images check the [KDE](https://osdn.net/projects/manjaro-archive/storage/kde/) archive.
