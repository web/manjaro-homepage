+++
Download_x64 = "https://download.manjaro.org/xfce/21.2.6/manjaro-xfce-21.2.6-220416-linux515.iso"
Download_x64_Checksum = "fac3e7c1db296c4edaa38ebb246eae27ae42f61e"
Download_x64_Sig = "https://download.manjaro.org/xfce/21.2.6/manjaro-xfce-21.2.6-220416-linux515.iso.sig"
Download_x64_Torrent = "https://download.manjaro.org/xfce/21.2.6/manjaro-xfce-21.2.6-220416-linux515.iso.torrent"

Download_Minimal_x64 = "https://download.manjaro.org/xfce/21.2.6/manjaro-xfce-21.2.6-minimal-220416-linux515.iso"
Download_Minimal_x64_Checksum = "88083c141ff3ec8fad7177624d6aec2f1e34c7fd"
Download_Minimal_x64_Sig = "https://download.manjaro.org/xfce/21.2.6/manjaro-xfce-21.2.6-minimal-220416-linux515.iso.sig"
Download_Minimal_x64_Torrent = "https://download.manjaro.org/xfce/21.2.6/manjaro-xfce-21.2.6-minimal-220416-linux515.iso.torrent"

Download_Minimal_lts = "https://download.manjaro.org/xfce/21.2.6/manjaro-xfce-21.2.6-minimal-220416-linux510.iso"
Download_Minimal_x64_Checksum_lts = "771f56eb33881d5b2fe5c8227d3d4f7fedce1d60"
Download_Minimal_x64_Sig_lts = "https://download.manjaro.org/xfce/21.2.6/manjaro-xfce-21.2.6-minimal-220416-linux510.iso.sig"
Download_Minimal_x64_Torrent_lts = "https://download.manjaro.org/xfce/21.2.6/manjaro-xfce-21.2.6-minimal-220416-linux510.iso.torrent"

Name = "XFCE"
Screenshot = "xfce-full.jpg"
Youtube = "I6WgT7JkkQk"
edition = "Official"
shortDescription = "For people who want a reliable and fast desktop"
Thumbnail = "xfce.jpg"
Version = "21.2.6"
date = "2022-04-16T05:38:06UTC"
title = "XFCE"
type="download-edition"
weight = "1"
meta_description = "Manjaro xfce for people who want a reliable and fast desktop"
meta_keywords = "manjaro xfce download, manjaro download"
+++

This edition is supported by the Manjaro team and comes with XFCE, a reliable desktop with high configurability.

Xfce is a lightweight desktop environment for UNIX-like operating systems. It aims to be fast and low on system resources, while still being visually appealing and user friendly. Xfce embodies the traditional UNIX philosophy of modularity and re-usability. It consists of a number of components that provide the full functionality one can expect of a modern desktop environment. They are packaged separately and you can pick among the available packages to create the optimal personal working environment.

If you are looking for older images check the [XFCE](https://osdn.net/projects/manjaro-archive/storage/xfce/) archive.


