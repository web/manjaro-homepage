+++
Download_x64 = "https://download.manjaro.org/gnome/21.2.6/manjaro-gnome-21.2.6-220416-linux515.iso"
Download_x64_Checksum = "20384822a9f7cec698dcd99a298c416b18597f17"
Download_x64_Sig = "https://download.manjaro.org/gnome/21.2.6/manjaro-gnome-21.2.6-220416-linux515.iso.sig"
Download_x64_Torrent = "https://download.manjaro.org/gnome/21.2.6/manjaro-gnome-21.2.6-220416-linux515.iso.torrent"

Download_Minimal_x64 = "https://download.manjaro.org/gnome/21.2.6/manjaro-gnome-21.2.6-minimal-220416-linux515.iso"
Download_Minimal_x64_Checksum = "bc3c218dce4f76f4035e08c3c123e82a8ed200fa"
Download_Minimal_x64_Sig = "https://download.manjaro.org/gnome/21.2.6/manjaro-gnome-21.2.6-minimal-220416-linux515.iso.sig"
Download_Minimal_x64_Torrent = "https://download.manjaro.org/gnome/21.2.6/manjaro-gnome-21.2.6-minimal-220416-linux515.iso.torrent"

Download_Minimal_lts = "https://download.manjaro.org/gnome/21.2.6/manjaro-gnome-21.2.6-minimal-220416-linux510.iso"
Download_Minimal_x64_Checksum_lts = "067854099b358a96575966a96025cfca4292574b"
Download_Minimal_x64_Sig_lts = "https://download.manjaro.org/gnome/21.2.6/manjaro-gnome-21.2.6-minimal-220416-linux510.iso.sig"
Download_Minimal_x64_Torrent_lts = "https://download.manjaro.org/gnome/21.2.6/manjaro-gnome-21.2.6-minimal-220416-linux510.iso.torrent"

Name = "GNOME"
Screenshot = "gnome-full.jpg"
Youtube = "i2U2oZ1b4RQ"
edition = "Official"
shortDescription = "For people who want a very modern and simple desktop"
Thumbnail = "gnome.jpg"
Version = "21.2.6"
date = "2022-04-16T05:38:06UTC"
title = "Gnome"
type="download-edition"
weight = "2"
meta_description = "Manjaro gnome for people who want a very modern and simple desktop"
meta_keywords = "manjaro gnome download, manjaro download"
+++

This edition is supported by the Manjaro team and comes with GNOME desktop that breaks from traditional concepts and allows users to focus on their tasks. Desktop-specific applications are crafted with care and clearly defined by guidelines that make them more consistent to use.

If you are looking for older images check the [GNOME](https://osdn.net/projects/manjaro-archive/storage/gnome/) archive.
