+++
Download_x64 = "https://github.com/manjaro-pinephone/phosh/releases/download/beta23/Manjaro-ARM-phosh-pinephone-beta23.img.xz"
Download_x64_Checksum = "a222691c6e67116d23c8f2f6bf79149f3bf09915"
Download_x64_Sig = ""
Download_x64_Torrent = "https://github.com/manjaro-pinephone/phosh/releases/download/beta23/Manjaro-ARM-phosh-pinephone-beta23.img.xz.torrent"
Download_x86 = ""
Download_x86_Checksum = ""
Download_x86_Sig = ""
Download_x86_Torrent = ""
Name = "Pinephone Phosh"
Screenshot = "arm-phosh-full.png"
Youtube = ""
edition = "ARM"
device = "Pinephone"
shortDescription = "Phosh is a mobile centric UI made by Purism based on Gnome."
Thumbnail = "arm-phosh-full.png"
Version = "Beta 23"
date = "02.2022"
title = "Pinephone Phosh"
type="download-edition"
weight = "1"
meta_description = "Phosh is a mobile centric UI made by Purism based on Gnome."
meta_keywords = "manjaro phosh arm, manjaro download"
+++

For people who look for a modern user experience.

This edition comes with Phosh, an interface based on GTK3 and GTK4, that has a slick and robust user experience.

Device Maintainer: Philip Mueller

Edition Maintainer: Philip Mueller
