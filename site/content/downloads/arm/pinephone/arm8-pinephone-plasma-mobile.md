+++
Download_x64 = "https://github.com/manjaro-pinephone/plasma-mobile/releases/download/beta11/Manjaro-ARM-plasma-mobile-pinephone-beta11.img.xz"
Download_x64_Checksum = "7ec7516656853207aac538f208dce78a36a4dcc6"
Download_x64_Sig = ""
Download_x64_Torrent = "https://github.com/manjaro-pinephone/plasma-mobile/releases/download/beta11/Manjaro-ARM-plasma-mobile-pinephone-beta11.img.xz.torrent"
Download_x86 = ""
Download_x86_Checksum = ""
Download_x86_Sig = ""
Download_x86_Torrent = ""
Name = "Pinephone Plasma Mobile"
Screenshot = "arm-plasma-mobile-full.png"
Youtube = ""
edition = "ARM"
device = "Pinephone"
shortDescription = "Plasma Mobile is a mobile centric UI made by KDE based on Plasma and Qt."
Thumbnail = "arm-plasma-mobile-full.png"
Version = "Beta 11"
date = "03.2022"
title = "Pinephone Plasma Mobile"
type="download-edition"
weight = "1"
meta_description = "Plasma Mobile is a mobile centric UI made by KDE based on Plasma and Qt."
meta_keywords = "manjaro plasma arm, manjaro download"
+++

This edition is supported by the Manjaro ARM team and comes with Plasma Mobile interface.

Plasma Mobile is for people who want a user-friendly and customizable interface for their mobile device. It is a feature-rich and versatile interface with the power of KWin and smoothness of Qt. Plasma Mobile is simple by default, a clean work area for real-world usage which intends to stay out of your way.

Device Maintainer: Philip Mueller

Edition Maintainer: Dan Johansen
