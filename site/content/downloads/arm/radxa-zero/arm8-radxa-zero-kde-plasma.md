+++
Download_x64 = "https://github.com/manjaro-arm/radxa-zero-images/releases/download/22.04/Manjaro-ARM-kde-plasma-radxa-zero-22.04.img.xz"
Download_x64_Checksum = "06094e8a343324997d222e42ea55e7064dbbd7f7"
Download_x64_Sig = ""
Download_x64_Torrent = "https://github.com/manjaro-arm/radxa-zero-images/releases/download/22.04/Manjaro-ARM-kde-plasma-radxa-zero-22.04.img.xz.torrent"
Download_x86 = ""
Download_x86_Checksum = ""
Download_x86_Sig = ""
Download_x86_Torrent = ""
Name = "Radxa Zero KDE Plasma"
Screenshot = "arm-kde-full.png"
Youtube = ""
edition = "ARM"
device = "Radxa Zero"
shortDescription = "Plasma on ARM is a full-featured desktop experience and all the bells and whistles you might want."
Thumbnail = "arm-kde-full.png"
Version = "22.04"
date = "04.2022"
title = "Radxa Zero KDE Plasma"
type="download-edition"
weight = "1"
meta_description = "Manjaro Plasma on ARM is a full-featured desktop experience and all the bells and whistles you might want."
meta_keywords = "manjaro kde arm, manjaro download"
+++

This edition is supported by the Manjaro ARM team and comes with Plasma desktop.

KDE Plasma is for people who want a user-friendly and customizable desktop. It is a feature-rich and versatile desktop environment that provides several different styles of menu to access applications. An excellent built-in interface to easily access and install new themes, widgets, etc, from the internet is also worth mentioning. KDE Plasma is simple by default, a clean work area for real-world usage which intends to stay out of your way, so the user is enabled to create the workflow that makes it more effective to complete tasks.

Device Maintainer: Furkan Kardame

Edition Maintainer: Dan Johansen
