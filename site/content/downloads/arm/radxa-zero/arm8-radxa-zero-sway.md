+++
Download_x64 = "https://github.com/manjaro-arm/radxa-zero-images/releases/download/22.04/Manjaro-ARM-sway-radxa-zero-22.04.img.xz"
Download_x64_Checksum = "249727f839dd5ca290892149335cd8b2ca1266a3"
Download_x64_Sig = ""
Download_x64_Torrent = "https://github.com/manjaro-arm/radxa-zero-images/releases/download/22.04/Manjaro-ARM-sway-radxa-zero-22.04.img.xz.torrent"
Download_x86 = ""
Download_x86_Checksum = ""
Download_x86_Sig = ""
Download_x86_Torrent = ""
Name = "Radxa Zero Sway"
Screenshot = "arm-sway-full.png"
Youtube = ""
edition = "ARM"
device = "Radxa Zero"
shortDescription = "Manjaro ARM with the lightweight tiling window manager sway."
Thumbnail = "arm-sway-full.png"
Version = "22.04"
date = "04.2022"
title = "Radxa Zero Sway"
type="download-edition"
weight = "1"
meta_description = "Manjaro sway on ARM is a lightweight tiling window manager for Wayland"
meta_keywords = "manjaro sway arm, manjaro download"
+++

This edition is supported by the Manjaro ARM team and comes with the sway tiling window manager.

Sway is an extremely lightweight tiling window manager for Wayland, famous for its efficiency with screen space and keyboard controlled workflow. The ideal environment for text and terminal focused usage for advanced users.

Device Maintainer: Furkan Kardame

Edition Maintainer: Andreas Gerlach
