+++
Download_x64 = "https://github.com/manjaro-arm/vim2-images/releases/download/22.04/Manjaro-ARM-minimal-vim2-22.04.img.xz"
Download_x64_Checksum = "40de3a9e7d4c536f1a1c95d5f4f102ff5937d25c"
Download_x64_Sig = ""
Download_x64_Torrent = "https://github.com/manjaro-arm/vim2-images/releases/download/22.04/Manjaro-ARM-minimal-vim2-22.04.img.xz.torrent"
Download_x86 = ""
Download_x86_Checksum = ""
Download_x86_Sig = ""
Download_x86_Torrent = ""
Name = "Khadas Vim 2 Minimal"
Screenshot = "arm-minimal-full.png"
Youtube = ""
edition = "ARM"
device = "Khadas Vim 2"
shortDescription = "For people that do not need a desktop"
Thumbnail = "arm-minimal-full.png"
Version = "22.04"
date = "04.2022"
title = "Khadas Vim 2 Minimal"
type="download-edition"
weight = "1"
meta_description = "Manjaro arm for people that do not need a desktop"
meta_keywords = "manjaro minimal arm, manjaro download"
+++

The Minimal edition is Manjaro ARM, but without a Desktop Environment or Xorg at all.

Device Maintainer: Furkan Kardame

Edition Maintainer: Dan Johansen
