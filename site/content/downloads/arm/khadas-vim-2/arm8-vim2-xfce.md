+++
Download_x64 = "https://github.com/manjaro-arm/vim2-images/releases/download/22.04/Manjaro-ARM-xfce-vim2-22.04.img.xz"
Download_x64_Checksum = "703a1371bee3aea48a6a561128df63208a73f5c3"
Download_x64_Sig = ""
Download_x64_Torrent = "https://github.com/manjaro-arm/vim2-images/releases/download/22.04/Manjaro-ARM-xfce-vim2-22.04.img.xz.torrent"
Download_x86 = ""
Download_x86_Checksum = ""
Download_x86_Sig = ""
Download_x86_Torrent = ""
Name = "Khadas Vim 2 XFCE"
Screenshot = "arm-xfce-full.png"
Youtube = ""
edition = "ARM"
device = "Khadas Vim 2"
shortDescription = "XFCE on ARM is one of the fastest DE's available and the most stable."
Thumbnail = "arm-xfce-full.png"
Version = "22.04"
date = "04.2022"
title = "Khadas Vim 2 XFCE"
type="download-edition"
weight = "1"
meta_description = "Manjaro XFCE on ARM is one of the fastest DE's available and the most stable."
meta_keywords = "manjaro xfce arm, manjaro download"
+++

This edition is supported by the Manjaro ARM team and comes with XFCE desktop.

XFCE is a lightweight, and very stable, GTK based desktop. It's modular and very customizable.

Device Maintainer: Furkan Kardame

Edition Maintainer: Ray Sherwin
