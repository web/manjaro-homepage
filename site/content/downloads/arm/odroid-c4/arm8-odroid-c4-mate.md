+++
Download_x64 = "https://github.com/manjaro-arm/oc4-images/releases/download/22.04/Manjaro-ARM-mate-oc4-22.04.img.xz"
Download_x64_Checksum = "15e1d89f0dc32971a1d874a45e614971c6820d4a"
Download_x64_Sig = ""
Download_x64_Torrent = "https://github.com/manjaro-arm/oc4-images/releases/download/22.04/Manjaro-ARM-mate-oc4-22.04.img.xz.torrent"
Download_x86 = ""
Download_x86_Checksum = ""
Download_x86_Sig = ""
Download_x86_Torrent = ""
Name = "Odroid C4 MATE"
Screenshot = "arm-mate-full.png"
Youtube = ""
edition = "ARM"
device = "Odroid C4"
shortDescription = "Manjaro MATE on ARM is a modern looking DE that follows the Gnome2 paradigm."
Thumbnail = "arm-mate-full.png"
Version = "22.04"
date = "04.2022"
title = "Odroid C4 MATE"
type="download-edition"
weight = "1"
meta_description = "Manjaro MATE on ARM is a modern looking DE that follows the Gnome2 paradigm."
meta_keywords = "manjaro mate arm, manjaro download"
+++

For people who look for a traditional experience

This edition comes with Mate, a desktop environment that continues the legacy of traditional user experience while carefully improving and modernizing it when needed.

Device Maintainer: Dan Johansen

Edition Maintainer: Ray Sherwin
