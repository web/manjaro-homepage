+++
Download_x64 = "https://github.com/manjaro-arm/ohc4-images/releases/download/22.04/Manjaro-ARM-xfce-ohc4-22.04.img.xz"
Download_x64_Checksum = "7bbb626c2ace76a28192987be267f09248d331a2"
Download_x64_Sig = ""
Download_x64_Torrent = "https://github.com/manjaro-arm/ohc4-images/releases/download/22.04/Manjaro-ARM-xfce-ohc4-22.04.img.xz.torrent"
Download_x86 = ""
Download_x86_Checksum = ""
Download_x86_Sig = ""
Download_x86_Torrent = ""
Name = "Odroid HC4 XFCE"
Screenshot = "arm-xfce-full.png"
Youtube = ""
edition = "ARM"
device = "Odroid HC4"
shortDescription = "XFCE on ARM is one of the fastest DE's available and the most stable."
Thumbnail = "arm-xfce-full.png"
Version = "22.04"
date = "04.2022"
title = "Odroid HC4 XFCE"
type="download-edition"
weight = "1"
meta_description = "Manjaro XFCE on ARM is one of the fastest DE's available and the most stable."
meta_keywords = "manjaro xfce arm, manjaro download"
+++

This edition is supported by the Manjaro ARM team and comes with XFCE desktop.

XFCE is a lightweight, and very stable, GTK based desktop. It's modular and very customizable.

Device Maintainer: Furkan Kardame

Edition Maintainer: Ray Sherwin
