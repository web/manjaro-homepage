+++
Download_x64 = "https://github.com/manjaro-arm/ohc4-images/releases/download/22.04/Manjaro-ARM-minimal-ohc4-22.04.img.xz"
Download_x64_Checksum = "e5fc13cf1f9fa7fbeed5ca8d9c07b32799088c85"
Download_x64_Sig = ""
Download_x64_Torrent = "https://github.com/manjaro-arm/ohc4-images/releases/download/22.04/Manjaro-ARM-minimal-ohc4-22.04.img.xz.torrent"
Download_x86 = ""
Download_x86_Checksum = ""
Download_x86_Sig = ""
Download_x86_Torrent = ""
Name = "Odroid HC4 Minimal"
Screenshot = "arm-minimal-full.png"
Youtube = ""
edition = "ARM"
device = "Odroid HC4"
shortDescription = "For people that do not need a desktop"
Thumbnail = "arm-minimal-full.png"
Version = "22.04"
date = "04.2022"
title = "Odroid HC4 Minimal"
type="download-edition"
weight = "1"
meta_description = "Manjaro minimal ARM for people that do not need a desktop"
meta_keywords = "manjaro minimal arm, manjaro download"
+++

The Minimal edition is Manjaro ARM, but without a Desktop Environment or Xorg at all.

Device Maintainer: Furkan Kardame

Edition Maintainer: Dan Johansen
