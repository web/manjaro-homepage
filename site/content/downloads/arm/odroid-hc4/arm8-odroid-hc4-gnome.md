+++
Download_x64 = "https://github.com/manjaro-arm/ohc4-images/releases/download/22.04/Manjaro-ARM-gnome-ohc4-22.04.img.xz"
Download_x64_Checksum = "913af51195d7c3a08a1dffa9c2f15ee2799df0b5"
Download_x64_Sig = ""
Download_x64_Torrent = "https://github.com/manjaro-arm/ohc4-images/releases/download/22.04/Manjaro-ARM-gnome-ohc4-22.04.img.xz.torrent"
Download_x86 = ""
Download_x86_Checksum = ""
Download_x86_Sig = ""
Download_x86_Torrent = ""
Name = "Odroid HC4 Gnome"
Screenshot = "arm-gnome-full.png"
Youtube = ""
edition = "ARM"
device = "Odroid HC4"
shortDescription = "Manjaro Gnome on ARM is a modern DE with a modern workflow."
Thumbnail = "arm-gnome-full.png"
Version = "22.04"
date = "04.2022"
title = "Odroid HC4 Gnome"
type="download-edition"
weight = "1"
meta_description = "Manjaro Gnome on ARM is a modern DE with a modern workflow."
meta_keywords = "manjaro gnome arm, manjaro download"
+++

For people who look for a modern user experience.

This edition comes with Gnome, a desktop based on GTK3 and GTK4, that has a slick user experience.

Device Maintainer: Furkan Kardame

Edition Maintainer: Andreas Gerlach
