+++
Download_x64 = "https://github.com/manjaro-arm/am6-plus-images/releases/download/22.04/Manjaro-ARM-xfce-am6-plus-22.04.img.xz"
Download_x64_Checksum = "ca9ff23b5e6af823e102c83c47d81c649837e5fc"
Download_x64_Sig = ""
Download_x64_Torrent = "https://github.com/manjaro-arm/am6-plus-images/releases/download/22.04/Manjaro-ARM-xfce-am6-plus-22.04.img.xz.torrent"
Download_x86 = ""
Download_x86_Checksum = ""
Download_x86_Sig = ""
Download_x86_Torrent = ""
Name = "Ugoos AM6 Plus XFCE"
Screenshot = "arm-xfce-full.png"
Youtube = ""
edition = "ARM"
device = "Ugoos AM6 Plus"
shortDescription = "XFCE on ARM is one of the fastest DE's available and the most stable."
Thumbnail = "arm-xfce-full.png"
Version = "22.04"
date = "04.2022"
title = "Ugoos AM6 Plus XFCE"
type="download-edition"
weight = "1"
meta_description = "Manjaro XFCE on ARM is one of the fastest DE's available and the most stable."
meta_keywords = "manjaro xfce arm, manjaro download"
+++

This edition is supported by the Manjaro ARM team and comes with XFCE desktop.

XFCE is a lightweight, and very stable, GTK based desktop. It's modular and very customizable.

Device Maintainer: Furkan Kardame

Edition Maintainer: Ray Sherwin
