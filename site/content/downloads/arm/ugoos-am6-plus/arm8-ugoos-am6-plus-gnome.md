+++
Download_x64 = "https://github.com/manjaro-arm/am6-plus-images/releases/download/22.04/Manjaro-ARM-gnome-am6-plus-22.04.img.xz"
Download_x64_Checksum = "737ed7de8a68d22690b5bb76d58c54899390f4d4"
Download_x64_Sig = ""
Download_x64_Torrent = "https://github.com/manjaro-arm/am6-plus-images/releases/download/22.04/Manjaro-ARM-gnome-am6-plus-22.04.img.xz.torrent"
Download_x86 = ""
Download_x86_Checksum = ""
Download_x86_Sig = ""
Download_x86_Torrent = ""
Name = "Ugoos AM6 Plus Gnome"
Screenshot = "arm-gnome-full.png"
Youtube = ""
edition = "ARM"
device = "Ugoos AM6 Plus"
shortDescription = "Manjaro Gnome on ARM is a modern DE with a modern workflow."
Thumbnail = "arm-gnome-full.png"
Version = "22.04"
date = "04.2022"
title = "Ugoos AM6 Plus Gnome"
type="download-edition"
weight = "1"
meta_description = "Manjaro Gnome on ARM is a modern DE with a modern workflow."
meta_keywords = "manjaro gnome arm, manjaro download"
+++

For people who look for a modern user experience.

This edition comes with Gnome, a desktop based on GTK3 and GTK4, that has a slick user experience.

Device Maintainer: Furkan Kardame

Edition Maintainer: Andreas Gerlach
