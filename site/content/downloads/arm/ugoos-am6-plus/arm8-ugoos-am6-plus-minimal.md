+++
Download_x64 = "https://github.com/manjaro-arm/am6-plus-images/releases/download/22.04/Manjaro-ARM-minimal-am6-plus-22.04.img.xz"
Download_x64_Checksum = "41120d543fbfd04bd6eb4cbac480a420729379da"
Download_x64_Sig = ""
Download_x64_Torrent = "https://github.com/manjaro-arm/am6-plus-images/releases/download/22.04/Manjaro-ARM-minimal-am6-plus-22.04.img.xz.torrent"
Download_x86 = ""
Download_x86_Checksum = ""
Download_x86_Sig = ""
Download_x86_Torrent = ""
Name = "Ugoos AM6 Plus Minimal"
Screenshot = "arm-minimal-full.png"
Youtube = ""
edition = "ARM"
device = "Ugoos AM6 Plus"
shortDescription = "For people that do not need a desktop"
Thumbnail = "arm-minimal-full.png"
Version = "22.04"
date = "04.2022"
title = "Ugoos AM6 Plus Minimal"
type="download-edition"
weight = "1"
meta_description = "Manjaro arm for people that do not need a desktop"
meta_keywords = "manjaro minimal arm, manjaro download"
+++

The Minimal edition is Manjaro ARM, but without a Desktop Environment or Xorg at all.

Device Maintainer: Furkan Kardame

Edition Maintainer: Dan Johansen
