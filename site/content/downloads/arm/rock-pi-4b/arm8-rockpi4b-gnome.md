+++
Download_x64 = "https://github.com/manjaro-arm/rockpi4b-images/releases/download/22.04/Manjaro-ARM-gnome-rockpi4b-22.04.img.xz"
Download_x64_Checksum = "2e6ad2fbbc7a2583b81b18bb7f66271c6f9b9857"
Download_x64_Sig = ""
Download_x64_Torrent = "https://github.com/manjaro-arm/rockpi4b-images/releases/download/22.04/Manjaro-ARM-gnome-rockpi4b-22.04.img.xz.torrent"
Download_x86 = ""
Download_x86_Checksum = ""
Download_x86_Sig = ""
Download_x86_Torrent = ""
Name = "Rock Pi 4B Gnome"
Screenshot = "arm-gnome-full.png"
Youtube = ""
edition = "ARM"
device = "Rock Pi 4B"
shortDescription = "Manjaro Gnome on ARM is a modern DE with a modern workflow."
Thumbnail = "arm-gnome-full.png"
Version = "22.04"
date = "04.2022"
title = "Rock Pi 4B Gnome"
type="download-edition"
weight = "1"
meta_description = "Manjaro Gnome on ARM is a modern DE with a modern workflow."
meta_keywords = "manjaro gnome arm, manjaro download"
+++

For people who look for a modern user experience.

This edition comes with Gnome, a desktop based on GTK3 and GTK4, that has a slick user experience.

Device Maintainer: Dan Johansen

Edition Maintainer: Andreas Gerlach
