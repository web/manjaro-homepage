+++
Download_x64 = "https://github.com/manjaro-arm/rockpi4b-images/releases/download/22.04/Manjaro-ARM-sway-rockpi4b-22.04.img.xz"
Download_x64_Checksum = "fb0f4a8deedfc9a77435062f658c0f9e92ae6834"
Download_x64_Sig = ""
Download_x64_Torrent = "https://github.com/manjaro-arm/rockpi4b-images/releases/download/22.04/Manjaro-ARM-sway-rockpi4b-22.04.img.xz.torrent"
Download_x86 = ""
Download_x86_Checksum = ""
Download_x86_Sig = ""
Download_x86_Torrent = ""
Name = "Rock Pi 4B Sway"
Screenshot = "arm-sway-full.png"
Youtube = ""
edition = "ARM"
device = "Rock Pi 4B"
shortDescription = "Manjaro ARM with the lightweight tiling window manager sway."
Thumbnail = "arm-sway-full.png"
Version = "22.04"
date = "04.2022"
title = "Rock Pi 4B Sway"
type="download-edition"
weight = "1"
meta_description = "Manjaro sway on ARM is a lightweight tiling window manager for Wayland"
meta_keywords = "manjaro sway arm, manjaro download"
+++

This edition is supported by the Manjaro ARM team and comes with the sway tiling window manager.

Sway is an extremely lightweight tiling window manager for Wayland, famous for its efficiency with screen space and keyboard controlled workflow. The ideal environment for text and terminal focused usage for advanced users.

Device Maintainer: Dan Johansen

Edition Maintainer: Andreas Gerlach
