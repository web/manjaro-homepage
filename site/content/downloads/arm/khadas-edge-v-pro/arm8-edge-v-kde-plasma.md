+++
Download_x64 = "https://osdn.net/projects/manjaro-arm/storage/edgev/kde-plasma/20.06/Manjaro-ARM-kde-plasma-edgev-20.06.img.xz"
Download_x64_Checksum = "0e53be1943c2f0b0d4785c1010aa9e63afa3f9d6"
Download_x64_Sig = "https://osdn.net/projects/manjaro-arm/storage/edgev/kde-plasma/20.06/Manjaro-ARM-kde-plasma-edgev-20.06.img.xz.sig"
Download_x64_Torrent = ""
Download_x86 = ""
Download_x86_Checksum = ""
Download_x86_Sig = ""
Download_x86_Torrent = ""
Name = "Khadas Edge-V Pro KDE Plasma"
Screenshot = "arm-kde-full.png"
Youtube = ""
edition = "ARM"
device = "Khadas Edge-V Pro"
shortDescription = "Plasma on ARM is a full-featured desktop experience and all the bells and whistles you might want."
Thumbnail = "arm-kde-full.png"
Version = "20.06"
date = "06.2020"
title = "Khadas Edge-V Pro KDE Plasma"
type="download-edition"
weight = "1"
meta_description = "Manjaro Plasma on ARM is a full-featured desktop experience and all the bells and whistles you might want."
meta_keywords = "manjaro kde arm, manjaro download"
+++

This edition is supported by the Manjaro ARM team and comes with Plasma desktop.

KDE Plasma is for people who want a user-friendly and customizable desktop. It is a feature-rich and versatile desktop environment that provides several different styles of menu to access applications. An excellent built-in interface to easily access and install new themes, widgets, etc, from the internet is also worth mentioning. KDE Plasma is simple by default, a clean work area for real-world usage which intends to stay out of your way, so the user is enabled to create the workflow that makes it more effective to complete tasks.
