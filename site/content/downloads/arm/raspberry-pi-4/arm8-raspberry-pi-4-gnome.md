+++
Download_x64 = "https://github.com/manjaro-arm/rpi4-images/releases/download/22.04/Manjaro-ARM-gnome-rpi4-22.04.img.xz"
Download_x64_Checksum = "8020e29e4c2fb764f17e8185ce8b5be759d5cff0"
Download_x64_Sig = ""
Download_x64_Torrent = "https://github.com/manjaro-arm/rpi4-images/releases/download/22.04/Manjaro-ARM-gnome-rpi4-22.04.img.xz.torrent"
Download_x86 = ""
Download_x86_Checksum = ""
Download_x86_Sig = ""
Download_x86_Torrent = ""
Name = "Raspberry Pi 4 Gnome"
Screenshot = "arm-gnome-full.png"
Youtube = ""
edition = "ARM"
device = "Raspberry Pi 4"
shortDescription = "Manjaro Gnome on ARM is a modern DE with a modern workflow."
Thumbnail = "arm-gnome-full.png"
Version = "22.04"
date = "04.2022"
title = "Raspberry Pi 4 Gnome"
type="download-edition"
weight = "1"
meta_description = "Manjaro Gnome on ARM is a modern DE with a modern workflow."
meta_keywords = "manjaro gnome arm, manjaro download"
+++

For people who look for a modern user experience.

This edition comes with Gnome, a desktop based on GTK3 and GTK4, that has a slick user experience.

Device Maintainer: Ray Sherwin

Edition Maintainer: Andreas Gerlach
