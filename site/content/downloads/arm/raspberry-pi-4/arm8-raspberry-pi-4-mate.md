+++
Download_x64 = "https://github.com/manjaro-arm/rpi4-images/releases/download/22.04/Manjaro-ARM-mate-rpi4-22.04.img.xz"
Download_x64_Checksum = "234c37b38208f7af5e162f74259024f0e03c7c79"
Download_x64_Sig = ""
Download_x64_Torrent = "https://github.com/manjaro-arm/rpi4-images/releases/download/22.04/Manjaro-ARM-mate-rpi4-22.04.img.xz.torrent"
Download_x86 = ""
Download_x86_Checksum = ""
Download_x86_Sig = ""
Download_x86_Torrent = ""
Name = "Raspberry Pi 4 MATE"
Screenshot = "arm-mate-full.png"
Youtube = ""
edition = "ARM"
device = "Raspberry Pi 4"
shortDescription = "Manjaro MATE on ARM is a modern looking DE that follows the Gnome2 paradigm."
Thumbnail = "arm-mate-full.png"
Version = "22.04"
date = "04.2022"
title = "Raspberry Pi 4 MATE"
type="download-edition"
weight = "1"
meta_description = "Manjaro MATE on ARM is a modern looking DE that follows the Gnome2 paradigm."
meta_keywords = "manjaro mate arm, manjaro download"
+++

For people who look for a traditional experience

This edition comes with Mate, a desktop environment that continues the legacy of traditional user experience while carefully improving and modernizing it when needed.

Device Maintainer: Ray Sherwin

Edition Maintainer: Ray Sherwin
