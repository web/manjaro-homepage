+++
Download_x64 = "https://github.com/manjaro-arm/rpi4-images/releases/download/22.04/Manjaro-ARM-kde-plasma-rpi4-22.04.img.xz"
Download_x64_Checksum = "8e06d9c35d70263aab51d44a3da3d91509038ae6"
Download_x64_Sig = ""
Download_x64_Torrent = "https://github.com/manjaro-arm/rpi4-images/releases/download/22.04/Manjaro-ARM-kde-plasma-rpi4-22.04.img.xz.torrent"
Download_x86 = ""
Download_x86_Checksum = ""
Download_x86_Sig = ""
Download_x86_Torrent = ""
Name = "Raspberry Pi 4 KDE Plasma"
Screenshot = "arm-kde-full.jpg"
Youtube = ""
edition = "ARM"
device = "Raspberry Pi 4"
shortDescription = "Plasma on ARM is a full-featured desktop experience and all the bells and whistles you might want."
Thumbnail = "arm-kde-full.png"
Version = "22.04"
date = "04.2022"
title = "Raspberry Pi 4 KDE Plasma"
type="download-edition"
weight = "1"
meta_description = "Manjaro Plasma on ARM is a full-featured desktop experience and all the bells and whistles you might want."
meta_keywords = "manjaro plasma arm, manjaro download"
+++

This edition is supported by the Manjaro ARM team and comes with Plasma desktop.

KDE is a feature-rich and versatile desktop environment that provides several different styles of menu to access applications. An excellent built-in interface to easily access and install new themes, widgets, etc, from the internet is also worth mentioning. While very user-friendly and certainly flashy, KDE is also quite resource heavy and noticably slower to start and use than a desktop environment such as XFCE. An Armv8 installation of Manjaro running KDE uses about 330 MB of memory.

Device Maintainer: Ray Sherwin

Edition Maintainer: Dan Johansen
