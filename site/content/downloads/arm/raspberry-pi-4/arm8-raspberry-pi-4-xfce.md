+++
Download_x64 = "https://github.com/manjaro-arm/rpi4-images/releases/download/22.04/Manjaro-ARM-xfce-rpi4-22.04.img.xz"
Download_x64_Checksum = "ce85aa1c2c34a11de02e16b5c9f4d3699fc3a82d"
Download_x64_Sig = ""
Download_x64_Torrent = "https://github.com/manjaro-arm/rpi4-images/releases/download/22.04/Manjaro-ARM-xfce-rpi4-22.04.img.xz.torrent"
Download_x86 = ""
Download_x86_Checksum = ""
Download_x86_Sig = ""
Download_x86_Torrent = ""
Name = "Raspberry Pi 4 XFCE"
Screenshot = "arm-xfce-full.png"
Youtube = ""
edition = "ARM"
device = "Raspberry Pi 4"
shortDescription = "XFCE on ARM is one of the fastest DE's available and the most stable."
Thumbnail = "arm-xfce-full.png"
Version = "22.04"
date = "04.2022"
title = "Raspberry Pi 4 XFCE"
type="download-edition"
weight = "1"
meta_description = "Manjaro XFCE on ARM is one of the fastest DE's available and the most stable."
meta_keywords = "manjaro xfce arm, manjaro download"
+++

This edition is supported by the Manjaro ARM team and comes with XFCE desktop.

XFCE is a lightweight, and very stable, GTK based desktop. It's modular and very customizable.

Device Maintainer: Ray Sherwin

Edition Maintainer: Ray Sherwin
