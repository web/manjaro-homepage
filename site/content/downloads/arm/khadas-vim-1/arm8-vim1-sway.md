+++
Download_x64 = "https://osdn.net/projects/manjaro-arm/storage/vim1/sway/20.10/Manjaro-ARM-sway-vim1-20.10.img.xz"
Download_x64_Checksum = "a81b1d227d2a4907a5a9837a3f841118397f0cdb"
Download_x64_Sig = "https://osdn.net/projects/manjaro-arm/storage/vim1/sway/20.10/Manjaro-ARM-sway-vim1-20.10.img.xz.sig"
Download_x64_Torrent = "https://osdn.net/projects/manjaro-arm/storage/vim1/sway/20.10/Manjaro-ARM-sway-vim1-20.10.img.xz.torrent"
Download_x86 = ""
Download_x86_Checksum = ""
Download_x86_Sig = ""
Download_x86_Torrent = ""
Name = "Khadas Vim 1 Sway"
Screenshot = "arm-sway-full.png"
Youtube = ""
edition = "ARM"
device = "Khadas Vim 1"
shortDescription = "Manjaro ARM with the lightweight tiling window manager sway."
Thumbnail = "arm-sway-full.png"
Version = "20.10"
date = "10.2020"
title = "Khadas Vim 1 Sway"
type="download-edition"
weight = "1"
meta_description = "Manjaro sway on ARM is a lightweight tiling window manager for Wayland"
meta_keywords = "manjaro sway arm, manjaro download"
+++

This edition is supported by the Manjaro ARM team and comes with the sway tiling window manager.

sway is an extremely lightweight tiling window manager for Wayland, famous for its efficiency with screen space and keyboard controlled workflow. The ideal environment for text and terminal focused usage for advanced users.
