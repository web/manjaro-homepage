+++
Download_x64 = "https://osdn.net/projects/manjaro-arm/storage/vim1/i3/20.10/Manjaro-ARM-i3-vim1-20.10.img.xz"
Download_x64_Checksum = "fe4ceae3fce30eb3008c6a4a773c443d1e8d1bda"
Download_x64_Sig = "https://osdn.net/projects/manjaro-arm/storage/vim1/i3/20.10/Manjaro-ARM-i3-vim1-20.10.img.xz.sig"
Download_x64_Torrent = "https://osdn.net/projects/manjaro-arm/storage/vim1/i3/20.10/Manjaro-ARM-i3-vim1-20.10.img.xz.torrent"
Download_x86 = ""
Download_x86_Checksum = ""
Download_x86_Sig = ""
Download_x86_Torrent = ""
Name = "Khadas Vim 1 I3"
Screenshot = "arm-i3-full.png"
Youtube = ""
edition = "ARM"
device = "Khadas Vim 1"
shortDescription = "Manjaro ARM with the lightweight tiling window manager I3."
Thumbnail = "arm-i3-full.png"
Version = "20.10"
date = "10.2020"
title = "Khadas Vim 1 I3"
type="download-edition"
weight = "1"
meta_description = "Manjaro i3 on ARM is a lightweight tiling window manager"
meta_keywords = "manjaro i3 arm, manjaro download"
+++

This edition is supported by the Manjaro ARM team and comes with the i3 tiling window manager.

I3 is an extremely lightweight tiling window manager, famous for its efficiency with screen space and keyboard controlled workflow. The ideal environment for text and terminal focused usage for advanced users.
