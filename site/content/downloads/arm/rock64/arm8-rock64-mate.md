+++
Download_x64 = "https://github.com/manjaro-arm/rock64-images/releases/download/22.04/Manjaro-ARM-mate-rock64-22.04.img.xz"
Download_x64_Checksum = "95d44f564d57c8b06253ee5a961c5a5ed8f50bad"
Download_x64_Sig = ""
Download_x64_Torrent = "https://github.com/manjaro-arm/rock64-images/releases/download/22.04/Manjaro-ARM-mate-rock64-22.04.img.xz.torrent"
Download_x86 = ""
Download_x86_Checksum = ""
Download_x86_Sig = ""
Download_x86_Torrent = ""
Name = "Rock64 MATE"
Screenshot = "arm-mate-full.png"
Youtube = ""
edition = "ARM"
device = "Rock64"
shortDescription = "Manjaro MATE on ARM is a modern looking DE that follows the Gnome2 paradigm."
Thumbnail = "arm-mate-full.png"
Version = "22.04"
date = "04.2022"
title = "Rock64 MATE"
type="download-edition"
weight = "1"
meta_description = "Manjaro MATE on ARM is a modern looking DE that follows the Gnome2 paradigm."
meta_keywords = "manjaro mate arm, manjaro download"
+++

For people who look for a traditional experience

This edition comes with Mate, a desktop environment that continues the legacy of traditional user experience while carefully improving and modernizing it when needed.

Device Maintainer: Dan Johansen

Edition Maintainer: Ray Sherwin
