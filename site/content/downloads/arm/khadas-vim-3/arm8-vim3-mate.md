+++
Download_x64 = "https://github.com/manjaro-arm/vim3-images/releases/download/22.04/Manjaro-ARM-mate-vim3-22.04.img.xz"
Download_x64_Checksum = "f78d2aacfff146ac1416f77648de72fb74ceffab"
Download_x64_Sig = ""
Download_x64_Torrent = "https://github.com/manjaro-arm/vim3-images/releases/download/22.04/Manjaro-ARM-mate-vim3-22.04.img.xz.torrent"
Download_x86 = ""
Download_x86_Checksum = ""
Download_x86_Sig = ""
Download_x86_Torrent = ""
Name = "Khadas Vim 3 MATE"
Screenshot = "arm-mate-full.png"
Youtube = ""
edition = "ARM"
device = "Khadas Vim 3"
shortDescription = "Manjaro MATE on ARM is a modern looking DE that follows the Gnome2 paradigm."
Thumbnail = "arm-mate-full.png"
Version = "22.04"
date = "04.2022"
title = "Khadas Vim 3 MATE"
type="download-edition"
weight = "1"
meta_description = "Manjaro MATE on ARM is a modern looking DE that follows the Gnome2 paradigm."
meta_keywords = "manjaro mate arm, manjaro download"
+++

For people who look for a traditional experience

This edition comes with Mate, a desktop environment that continues the legacy of traditional user experience while carefully improving and modernizing it when needed.

Device Maintainer: Furkan Kardame

Edition Maintainer: Ray Sherwin
