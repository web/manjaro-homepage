+++
Download_x64 = "https://github.com/manjaro-arm/generic-images/releases/download/22.04/Manjaro-ARM-mate-generic-22.04.img.xz"
Download_x64_Checksum = "f560966ce013bdce5ed04620a27a6e7bd081f5ac"
Download_x64_Sig = ""
Download_x64_Torrent = "https://github.com/manjaro-arm/generic-images/releases/download/22.04/Manjaro-ARM-mate-generic-22.04.img.xz.torrent"
Download_x86 = ""
Download_x86_Checksum = ""
Download_x86_Sig = ""
Download_x86_Torrent = ""
Name = "Generic Aarch64 MATE"
Screenshot = "arm-mate-full.png"
Youtube = ""
edition = "ARM"
device = "Generic"
shortDescription = "Manjaro MATE on ARM is a modern looking DE that follows the Gnome2 paradigm."
Thumbnail = "arm-mate-full.png"
Version = "22.04"
date = "04.2022"
title = "Generic Aarch64 MATE"
type="download-edition"
weight = "1"
meta_description = "Manjaro MATE on ARM is a modern looking DE that follows the Gnome2 paradigm."
meta_keywords = "manjaro mate arm, manjaro download"
+++

For people who look for a traditional experience

This edition comes with Mate, a desktop environment that continues the legacy of traditional user experience while carefully improving and modernizing it when needed.

Device Maintainer: Dan Johansen

Edition Maintainer: Ray Sherwin

Prerequisites:

This image assumes that your board/device has working Board Firmware flashed to it's SPI flash space. This can either be U-boot or Tow-boot. We recommend Tow-boot as it's really easy to flash and presents a handy menu.
If Tow-boot supports your device, you can download an spi-imager.img for your device on the projects github page: https://github.com/Tow-Boot/Tow-Boot/releases.
Flash the correct spi-imager.img to an SD card and boot your device from it. Select the "Flash to SPI" option in the menu. Once successful, you can flash this generic image to any drive at boot it on the device.

Tested Devices:

Pine64 LTS (tested by Dan Johansen): 
For better support, install the "pinebook-post-install" package.

Pinebook Pro (tested by Furkan Kardame): 
For better support, install the "pinebookpro-post-install" package.

Odroid N2(+) (tested by Dan Johansen): 
For better support, install the "on2-post-install" package.
