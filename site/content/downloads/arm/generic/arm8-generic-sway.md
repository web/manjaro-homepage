+++
Download_x64 = "https://github.com/manjaro-arm/generic-images/releases/download/22.04/Manjaro-ARM-sway-generic-22.04.img.xz"
Download_x64_Checksum = "e873137be74dba77eb1ca0a7f6390819f9868122"
Download_x64_Sig = ""
Download_x64_Torrent = "https://github.com/manjaro-arm/generic-images/releases/download/22.04/Manjaro-ARM-sway-generic-22.04.img.xz.torrent"
Download_x86 = ""
Download_x86_Checksum = ""
Download_x86_Sig = ""
Download_x86_Torrent = ""
Name = "Generic Aarch64 Sway"
Screenshot = "arm-sway-full.png"
Youtube = ""
edition = "ARM"
device = "Generic"
shortDescription = "Manjaro ARM with the lightweight tiling window manager sway."
Thumbnail = "arm-sway-full.png"
Version = "22.04"
date = "04.2022"
title = "Generic Aarch64 Sway"
type="download-edition"
weight = "1"
meta_description = "Manjaro sway on ARM is a lightweight tiling window manager for Wayland"
meta_keywords = "manjaro sway arm, manjaro download"
+++

This edition is supported by the Manjaro ARM team and comes with the sway tiling window manager.

Sway is an extremely lightweight tiling window manager for Wayland, famous for its efficiency with screen space and keyboard controlled workflow. The ideal environment for text and terminal focused usage for advanced users.

Device Maintainer: Dan Johansen

Edition Maintainer: Andreas Gerlach

Prerequisites:

This image assumes that your board/device has working Board Firmware flashed to it's SPI flash space. This can either be U-boot or Tow-boot. We recommend Tow-boot as it's really easy to flash and presents a handy menu.
If Tow-boot supports your device, you can download an spi-imager.img for your device on the projects github page: https://github.com/Tow-Boot/Tow-Boot/releases.
Flash the correct spi-imager.img to an SD card and boot your device from it. Select the "Flash to SPI" option in the menu. Once successful, you can flash this generic image to any drive at boot it on the device.

Tested Devices:

Pine64 LTS (tested by Dan Johansen): 
For better support, install the "pinebook-post-install" package.

Pinebook Pro (tested by Furkan Kardame): 
For better support, install the "pinebookpro-post-install" package.

Odroid N2(+) (tested by Dan Johansen): 
For better support, install the "on2-post-install" package.
