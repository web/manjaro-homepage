+++
Download_x64 = "https://github.com/manjaro-arm/generic-images/releases/download/22.04/Manjaro-ARM-gnome-generic-22.04.img.xz"
Download_x64_Checksum = "39108880711349af6040a51402fcfbcb7d9fcae9"
Download_x64_Sig = ""
Download_x64_Torrent = "https://github.com/manjaro-arm/generic-images/releases/download/22.04/Manjaro-ARM-gnome-generic-22.04.img.xz.torrent"
Download_x86 = ""
Download_x86_Checksum = ""
Download_x86_Sig = ""
Download_x86_Torrent = ""
Name = "Generic Aarch64 Gnome"
Screenshot = "arm-gnome-full.png"
Youtube = ""
edition = "ARM"
device = "Generic"
shortDescription = "Manjaro Gnome on ARM is a modern DE with a modern workflow."
Thumbnail = "arm-gnome-full.png"
Version = "22.04"
date = "04.2022"
title = "Generic Aarch64 Gnome"
type="download-edition"
weight = "1"
meta_description = "Manjaro Gnome on ARM is a modern DE with a modern workflow."
meta_keywords = "manjaro gnome arm, manjaro download"
+++

For people who look for a modern user experience.

This edition comes with Gnome, a desktop based on GTK3 and GTK4, that has a slick user experience.

Device Maintainer: Dan Johansen

Edition Maintainer: Andreas Gerlach

Prerequisites:

This image assumes that your board/device has working Board Firmware flashed to it's SPI flash space. This can either be U-boot or Tow-boot. We recommend Tow-boot as it's really easy to flash and presents a handy menu.
If Tow-boot supports your device, you can download an spi-imager.img for your device on the projects github page: https://github.com/Tow-Boot/Tow-Boot/releases.
Flash the correct spi-imager.img to an SD card and boot your device from it. Select the "Flash to SPI" option in the menu. Once successful, you can flash this generic image to any drive at boot it on the device.

Tested Devices:

Pine64 LTS (tested by Dan Johansen): 
For better support, install the "pinebook-post-install" package.

Pinebook Pro (tested by Furkan Kardame): 
For better support, install the "pinebookpro-post-install" package.

Odroid N2(+) (tested by Dan Johansen): 
For better support, install the "on2-post-install" package.
