+++
Download_x64 = "https://github.com/manjaro-arm/generic-images/releases/download/22.04/Manjaro-ARM-kde-plasma-generic-22.04.img.xz"
Download_x64_Checksum = "bc08ad57ff70fa1d762b3e086ffebf86014270da"
Download_x64_Sig = ""
Download_x64_Torrent = "https://github.com/manjaro-arm/generic-images/releases/download/22.04/Manjaro-ARM-kde-plasma-generic-22.04.img.xz.torrent"
Download_x86 = ""
Download_x86_Checksum = ""
Download_x86_Sig = ""
Download_x86_Torrent = ""
Name = "Generic Aarch64 KDE Plasma"
Screenshot = "arm-kde-full.png"
Youtube = ""
edition = "ARM"
device = "Generic"
shortDescription = "Plasma on ARM is a full-featured desktop experience and all the bells and whistles you might want."
Thumbnail = "arm-kde-full.png"
Version = "22.04"
date = "04.2022"
title = "Generic Aarch64 KDE Plasma"
type="download-edition"
weight = "1"
meta_description = "Manjaro Plasma on ARM is a full-featured desktop experience and all the bells and whistles you might want."
meta_keywords = "manjaro kde arm, manjaro download"
+++

This edition is supported by the Manjaro ARM team and comes with Plasma desktop.

KDE Plasma is for people who want a user-friendly and customizable desktop. It is a feature-rich and versatile desktop environment that provides several different styles of menu to access applications. An excellent built-in interface to easily access and install new themes, widgets, etc, from the internet is also worth mentioning. KDE Plasma is simple by default, a clean work area for real-world usage which intends to stay out of your way, so the user is enabled to create the workflow that makes it more effective to complete tasks.

Device Maintainer: Dan Johansen

Edition Maintainer: Dan Johansen

Prerequisites:

This image assumes that your board/device has working Board Firmware flashed to it's SPI flash space. This can either be U-boot or Tow-boot. We recommend Tow-boot as it's really easy to flash and presents a handy menu.
If Tow-boot supports your device, you can download an spi-imager.img for your device on the projects github page: https://github.com/Tow-Boot/Tow-Boot/releases.
Flash the correct spi-imager.img to an SD card and boot your device from it. Select the "Flash to SPI" option in the menu. Once successful, you can flash this generic image to any drive at boot it on the device.

Tested Devices:

Pine64 LTS (tested by Dan Johansen): 
For better support, install the "pinebook-post-install" package.

Pinebook Pro (tested by Furkan Kardame): 
For better support, install the "pinebookpro-post-install" package.

Odroid N2(+) (tested by Dan Johansen): 
For better support, install the "on2-post-install" package.
