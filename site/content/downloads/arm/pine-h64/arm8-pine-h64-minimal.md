+++
Download_x64 = "https://github.com/manjaro-arm/pine-h64-images/releases/download/22.04/Manjaro-ARM-minimal-pine-h64-22.04.img.xz"
Download_x64_Checksum = "63406723c8ff4c0aee01dc794ac286724d58047e"
Download_x64_Sig = ""
Download_x64_Torrent = "https://github.com/manjaro-arm/pine-h64-images/releases/download/22.04/Manjaro-ARM-minimal-pine-h64-22.04.img.xz.torrent"
Download_x86 = ""
Download_x86_Checksum = ""
Download_x86_Sig = ""
Download_x86_Torrent = ""
Name = "Pine H64 Minimal"
Screenshot = "arm-minimal-full.png"
Youtube = ""
edition = "ARM"
device = "Pine H64"
shortDescription = "For people that do not need a desktop"
Thumbnail = "arm-minimal-full.png"
Version = "22.04"
date = "04.2022"
title = "Pine H64 Minimal"
type="download-edition"
weight = "1"
meta_description = "Manjaro minimal ARM for people that do not need a desktop"
meta_keywords = "manjaro minimal arm, manjaro download"
+++

The Minimal edition is Manjaro ARM, but without a Desktop Environment or Xorg at all.

Device Maintainer: Dan Johansen

Edition Maintainer: Dan Johansen
