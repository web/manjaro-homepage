+++
Download_x64 = "https://github.com/manjaro-arm/on2-images/releases/download/22.04/Manjaro-ARM-minimal-on2-22.04.img.xz"
Download_x64_Checksum = "9c30150b5a84121a383bcc89378d9b25d3b6548e"
Download_x64_Sig = ""
Download_x64_Torrent = "https://github.com/manjaro-arm/on2-images/releases/download/22.04/Manjaro-ARM-minimal-on2-22.04.img.xz.torrent"
Download_x86 = ""
Download_x86_Checksum = ""
Download_x86_Sig = ""
Download_x86_Torrent = ""
Name = "Odroid N2 Minimal"
Screenshot = "arm-minimal-full.png"
Youtube = ""
edition = "ARM"
device = "Odroid N2"
shortDescription = "For people that do not need a desktop"
Thumbnail = "arm-minimal-full.png"
Version = "22.04"
date = "04.2022"
title = "Odroid N2 Minimal"
type="download-edition"
weight = "1"
meta_description = "Manjaro minimal ARM for people that do not need a desktop"
meta_keywords = "manjaro minimal arm, manjaro download"
+++

The Minimal edition is Manjaro ARM, but without a Desktop Environment or Xorg at all.

Device Maintainer: Dan Johansen

Edition Maintainer: Dan Johansen
