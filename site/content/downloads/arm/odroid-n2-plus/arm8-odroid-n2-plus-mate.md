+++
Download_x64 = "https://github.com/manjaro-arm/on2-plus-images/releases/download/22.04/Manjaro-ARM-mate-on2-plus-22.04.img.xz"
Download_x64_Checksum = "4599f20dddce4d6aff63197c2b0ae2f7701d2f1f"
Download_x64_Sig = ""
Download_x64_Torrent = "https://github.com/manjaro-arm/on2-plus-images/releases/download/22.04/Manjaro-ARM-mate-on2-plus-22.04.img.xz.torrent"
Download_x86 = ""
Download_x86_Checksum = ""
Download_x86_Sig = ""
Download_x86_Torrent = ""
Name = "Odroid N2+ MATE"
Screenshot = "arm-mate-full.png"
Youtube = ""
edition = "ARM"
device = "Odroid N2+"
shortDescription = "Manjaro MATE on ARM is a modern looking DE that follows the Gnome2 paradigm."
Thumbnail = "arm-mate-full.png"
Version = "22.04"
date = "04.2022"
title = "Odroid N2+ MATE"
type="download-edition"
weight = "1"
meta_description = "Manjaro MATE on ARM is a modern looking DE that follows the Gnome2 paradigm."
meta_keywords = "manjaro mate arm, manjaro download"
+++

For people who look for a traditional experience

This edition comes with Mate, a desktop environment that continues the legacy of traditional user experience while carefully improving and modernizing it when needed.

Device Maintainer: Dan Johansen

Edition Maintainer: Ray Sherwin
