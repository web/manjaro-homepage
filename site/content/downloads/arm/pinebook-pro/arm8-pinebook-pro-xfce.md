+++
Download_x64 = "https://github.com/manjaro-arm/pbpro-images/releases/download/22.04/Manjaro-ARM-xfce-pbpro-22.04.img.xz"
Download_x64_Checksum = "31bef802d6bac861e9031273b5a8cf1171417572"
Download_x64_Sig = ""
Download_x64_Torrent = "https://github.com/manjaro-arm/pbpro-images/releases/download/22.04/Manjaro-ARM-xfce-pbpro-22.04.img.xz.torrent"
Download_x86 = ""
Download_x86_Checksum = ""
Download_x86_Sig = ""
Download_x86_Torrent = ""
Name = "Pinebook Pro XFCE"
Screenshot = "arm-xfce-full.png"
Youtube = ""
edition = "ARM"
device = "Pinebook Pro"
shortDescription = "XFCE on ARM is one of the fastest DE's available and the most stable."
Thumbnail = "arm-xfce-full.png"
Version = "22.04"
date = "04.2022"
title = "Pinebook Pro XFCE"
type="download-edition"
weight = "1"
meta_description = "Manjaro XFCE on ARM is one of the fastest DE's available and the most stable."
meta_keywords = "manjaro xfce arm, manjaro download"
+++

This edition is supported by the Manjaro ARM team and comes with XFCE desktop.

XFCE is a lightweight, and very stable, GTK based desktop. It's modular and very customizable.

Device Maintainer: Dan Johansen

Edition Maintainer: Ray Sherwin
