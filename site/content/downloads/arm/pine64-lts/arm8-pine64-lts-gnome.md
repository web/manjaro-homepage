+++
Download_x64 = "https://github.com/manjaro-arm/pine64-lts-images/releases/download/22.04/Manjaro-ARM-gnome-pine64-lts-22.04.img.xz"
Download_x64_Checksum = "4a0fed5d3fa3acdf2cca34f18d38be60024b3af5"
Download_x64_Sig = ""
Download_x64_Torrent = "https://github.com/manjaro-arm/pine64-lts-images/releases/download/22.04/Manjaro-ARM-gnome-pine64-lts-22.04.img.xz.torrent"
Download_x86 = ""
Download_x86_Checksum = ""
Download_x86_Sig = ""
Download_x86_Torrent = ""
Name = "Pine64 LTS Gnome"
Screenshot = "arm-gnome-full.png"
Youtube = ""
edition = "ARM"
device = "Pine64 LTS"
shortDescription = "Manjaro Gnome on ARM is a modern DE with a modern workflow."
Thumbnail = "arm-gnome-full.png"
Version = "22.04"
date = "04.2022"
title = "Pine64 LTS Gnome"
type="download-edition"
weight = "1"
meta_description = "Manjaro Gnome on ARM is a modern DE with a modern workflow."
meta_keywords = "manjaro gnome arm, manjaro download"
+++

For people who look for a modern user experience.

This edition comes with Gnome, a desktop based on GTK3 and GTK4, that has a slick user experience.

Device Maintainer: Dan Johansen

Edition Maintainer: Andreas Gerlach
