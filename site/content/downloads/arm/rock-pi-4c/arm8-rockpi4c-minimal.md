+++
Download_x64 = "https://github.com/manjaro-arm/rockpi4c-images/releases/download/22.04/Manjaro-ARM-minimal-rockpi4c-22.04.img.xz"
Download_x64_Checksum = "948cf8a936875119553417996603933e5f57d0c5"
Download_x64_Sig = ""
Download_x64_Torrent = "https://github.com/manjaro-arm/rockpi4c-images/releases/download/22.04/Manjaro-ARM-minimal-rockpi4c-22.04.img.xz.torrent"
Download_x86 = ""
Download_x86_Checksum = ""
Download_x86_Sig = ""
Download_x86_Torrent = ""
Name = "Rock Pi 4C Minimal"
Screenshot = "arm-minimal-full.png"
Youtube = ""
edition = "ARM"
device = "Rock Pi 4C"
shortDescription = "For people that do not need a desktop"
Thumbnail = "arm-minimal-full.png"
Version = "22.04"
date = "04.2022"
title = "Rock Pi 4C Minimal"
type="download-edition"
weight = "1"
meta_description = "Manjaro minimal ARM for people that do not need a desktop"
meta_keywords = "manjaro minimal arm, manjaro download"
+++

The Minimal edition is Manjaro ARM, but without a Desktop Environment or Xorg at all.

Device Maintainer: Dan Johansen

Edition Maintainer: Dan Johansen
