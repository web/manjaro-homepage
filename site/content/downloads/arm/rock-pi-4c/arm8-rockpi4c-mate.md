+++
Download_x64 = "https://github.com/manjaro-arm/rockpi4c-images/releases/download/22.04/Manjaro-ARM-mate-rockpi4c-22.04.img.xz"
Download_x64_Checksum = "799f84c6c91fcba0a9b26b777541079ab9904f91"
Download_x64_Sig = ""
Download_x64_Torrent = "https://github.com/manjaro-arm/rockpi4c-images/releases/download/22.04/Manjaro-ARM-mate-rockpi4c-22.04.img.xz.torrent"
Download_x86 = ""
Download_x86_Checksum = ""
Download_x86_Sig = ""
Download_x86_Torrent = ""
Name = "Rock Pi 4C MATE"
Screenshot = "arm-mate-full.png"
Youtube = ""
edition = "ARM"
device = "Rock Pi 4C"
shortDescription = "Manjaro MATE on ARM is a modern looking DE that follows the Gnome2 paradigm."
Thumbnail = "arm-mate-full.png"
Version = "22.04"
date = "04.2022"
title = "Rock Pi 4C MATE"
type="download-edition"
weight = "1"
meta_description = "Manjaro MATE on ARM is a modern looking DE that follows the Gnome2 paradigm."
meta_keywords = "manjaro mate arm, manjaro download"
+++

For people who look for a traditional experience

This edition comes with Mate, a desktop environment that continues the legacy of traditional user experience while carefully improving and modernizing it when needed.

Device Maintainer: Dan Johansen

Edition Maintainer: Ray Sherwin
