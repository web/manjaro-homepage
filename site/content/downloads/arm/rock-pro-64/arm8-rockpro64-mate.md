+++
Download_x64 = "https://github.com/manjaro-arm/rockpro64-images/releases/download/22.04/Manjaro-ARM-mate-rockpro64-22.04.img.xz"
Download_x64_Checksum = "48fd3cfc3c1b9bae4825ad88eb9c7e24772a3b58"
Download_x64_Sig = ""
Download_x64_Torrent = "https://github.com/manjaro-arm/rockpro64-images/releases/download/22.04/Manjaro-ARM-mate-rockpro64-22.04.img.xz.torrent"
Download_x86 = ""
Download_x86_Checksum = ""
Download_x86_Sig = ""
Download_x86_Torrent = ""
Name = "RockPro64 MATE"
Screenshot = "arm-mate-full.png"
Youtube = ""
edition = "ARM"
device = "RockPro64"
shortDescription = "Manjaro MATE on ARM is a modern looking DE that follows the Gnome2 paradigm."
Thumbnail = "arm-mate-full.png"
Version = "22.04"
date = "04.2022"
title = "RockPro64 MATE"
type="download-edition"
weight = "1"
meta_description = "Manjaro MATE on ARM is a modern looking DE that follows the Gnome2 paradigm."
meta_keywords = "manjaro mate arm, manjaro download"
+++

For people who look for a traditional experience

This edition comes with Mate, a desktop environment that continues the legacy of traditional user experience while carefully improving and modernizing it when needed.

Device Maintainer: Dan Johansen

Edition Maintainer: Ray Sherwin
