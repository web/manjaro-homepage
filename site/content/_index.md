+++ 
class = "landing-page" 
background = "/img/bg.jpg" 
title =  "OPERATING SYSTEM FOR EVERYONE" 
whatIsManjaro = "Manjaro is an open source operating system that can be installed on Desktops/Laptops/Tablets or Mobile Phones. Different user interfaces are available." 
description = "description of the page for search engine meta tags"
facebook = "https://www.facebook.com/ManjaroLinux"
twitter = "https://twitter.com/ManjaroLinux"
youtube = "https://www.youtube.com/channel/UCdGFLV7h9RGeTUX7wa5rqGw"
gitlab = "https://gitlab.manjaro.org"
+++


