{
  "date": "2016-09-05T15:08:00+02:00",
  "image": "developers.jpg",
  "screenshot": "developers-alt-full.jpg",
  "icon" : "developers-icon.svg",
  "name": "Developers",
  "quote": "MANJARO, Its an easy to use \"Arch\"itechture that will be admired by both; the first timers as well as professionals / programmers..... it just works !",
  "quoteauthor": "rvc",
  "shortdescription": "Developers want a system that does one thing: be efficient, so there's more time for the actual development. We promise that Manjaro will make development fun, efficient and easier.",
  "title": "Developers",
  "type": "usercase-post",
  "software" : [
 
  ],
  "softwarepresented" : [
  {"name" : "Visual Studio Code", "url" : "https://software.manjaro.org/package/code", "usage" : "Code editing" },
  {"name" : "KDevelop", "url" : "https://software.manjaro.org/package/kdevelop", "usage" : "IDE" },
  {"name" : "Geany", "url" : "https://software.manjaro.org/package/geany", "usage" : "Text-Editor" }
  ]
}

Manjaro comes in many different editions, all with a unique workflow and user experience from a traditional desktop environment to window managers that are optimized for maximum performance and work efficiency.

The system is set up easily, while giving developers all the freedom and space to tinker when wanted. All tools to compile and develop software are pre installed. The integrated package manager allows easy installation of IDEs like [Qt Creator](https://software.manjaro.org/package/qtcreator), [KDevelop](https://software.manjaro.org/package/kdevelop) or [Netbeans](https://software.manjaro.org/package/netbeans) and libraries like [libnoise](http://libnoise.sourceforge.net/), [boost](https://software.manjaro.org/package/boost-libs) or [matplotlib](https://software.manjaro.org/package/python-matplotlib).

Dependencies are automatically resolved and installing a library automatically installs all needed headers to directly use it. Each edition comes preinstalled with powerful editors like [Kate](https://software.manjaro.org/package/kate) or [Gedit](https://software.manjaro.org/package/gedit), while others like [Emacs](https://software.manjaro.org/package/emacs), [Geany](https://software.manjaro.org/package/geany) or [Bluefish](https://software.manjaro.org/package/bluefish) can be installed with one command or a few clicks.

Additionaly to the repositories, Manjaro supports the Arch User Repository (AUR) that includes cutting edge software and libraries.

Manjaro is a rolling release distribution, which means software is updated as soon as possible. This prevents lack of needed dependencies and old or buggy tool versions.
