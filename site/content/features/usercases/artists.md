{
  "date": "2016-09-05T15:07:44+02:00",
  "image": "artists-alt2.jpg",
  "screenshot": "artists-alt-full.jpg",
  "icon" : "artists-icon.svg",
  "name": "Artists",
  "quote": "Manjaro provides the tools to handle difficult tasks like kernel, video driver upgrades, downgrades. [...]  I soon realised I can do all my editing on linux with Darktable for raw files and some retouching or other editing with Gimp.",
  "quoteauthor": "louis_2",
  "shortdescription": "Artists make the graphics we see, the music we hear, and document our lifestyles and places. Manjaro brings all tools for artists, whether being pre-installed or easily installable via a comfortable and secure package manager.",
  "title": "Artists",
  "type": "usercase-post",
  "software" : [
  
  ],
  "softwarepresented" : [
  {"name" : "Krita", "url" : "https://software.manjaro.org/package/krita", "usage" : "Graphics editing, drawing" },
  {"name" : "Kdenlive", "url" : "https://software.manjaro.org/package/kdenlive", "usage" : "Video editing" },
  {"name" : "Ardour", "url" : "https://software.manjaro.org/package/ardour", "usage" : "Sound recording, mixing & editing" }
  ]
}

Manjaro comes preinstalled with [GIMP](https://software.manjaro.org/package/gimp), a very powerful image manipulation program. For drawing [Krita](https://software.manjaro.org/package/krita) will fulfill most of your needs, while [Darktable](https://software.manjaro.org/package/darktable) brings a top-notch and free photography suite. Vector-graphics can be done with programs such as [Inkscape](https://software.manjaro.org/package/inkscape).

For simple or advanced video-editing, [Kdenlive](https://software.manjaro.org/package/kdenlive) and [OpenShot](https://software.manjaro.org/package/openshot) will do almost anything you need.

3D-Modelling can be done using [Blender](https://software.manjaro.org/package/blender). Codecs for video and audio come pre-installed or are automatically fetched. Most of this software is freely available and can be easily and securely installed using Manjaro's integrated package manager.

Manjaro repositories also contain software useful for musicians such as the easy to use recording and editing software [Audacity](https://software.manjaro.org/package/audacity). For more professional users [Ardour](https://software.manjaro.org/package/ardour) brings all the needed features. Sequencing, mixing and synthesizing can be done using [LMMS](https://software.manjaro.org/package/lmms). Manjaro maintains [realtime](https://forum.manjaro.org/t/a-realtime-kernel-for-manjaro/4066) versions of the Linux kernel that are designed for low latency and optimized for audio-engineering.
With [Musescore](https://software.manjaro.org/package/musescore) you have a professional tool for creating, playing and printing scores.
