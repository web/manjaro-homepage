{
  "date": "2016-09-05T15:08:09+02:00",
  "image": "gamers-alt1.jpg",
  "screenshot": "gamers-alt-full.jpg",
  "icon" : "gamers-icon.svg",
  "name": "Gamers",
  "quote": "Finally, Manjaro is a great balance between having the latest and greatest software and having a stable system. [...] Not to mention that Manjaro will only download updates and upgrade when I tell it to and not when the operating system feels like it.",
  "quoteauthor": "Bob1234",
  "shortdescription": "Gamers want their system to be reliable, efficent and able to handle all their games with great performance, updating drivers or applications is a smooth experience letting you be up and running in just a couple of minutes to play your favorite titles.",
  "title": "Gamers",
  "type": "usercase-post",
  "software" : [
  
  ],
  "softwarepresented" : [
  {"name" : "Steam", "url" : "https://software.manjaro.org/package/steam-manjaro", "usage" : "Software store" },
  {"name" : "0 A.D.", "url" : "https://software.manjaro.org/package/0ad", "usage" : "Strategy game" },
  {"name" : "PlayOnLinux", "url" : "https://software.manjaro.org/package/playonlinux", "usage" : "Play Windows games" }
  ]
}

Manjaro comes preinstalled with [Steam](https://software.manjaro.org/package/steam-manjaro), so there's no need to go to a website and download it manually. Installing drivers, updates and more applications is handled through the package manager and [Manjaro Hardware Detection Tool](https://wiki.manjaro.org/index.php/Manjaro_Hardware_Detection) that will do all operations in just a couple of minutes and never requires or forces a restart of the system, unless you update the kernel.

Due to the design of Linux, hardware like Gamepads are recognized instantaneously if drivers are available, so users don't have to wait until it works.

Included in the package repositories is software like [Teamspeak 3](https://software.manjaro.org/package/teamspeak3), [Mumble](https://software.manjaro.org/package/mumble) or [Skype](https://software.manjaro.org/package/skype-online-jak). Games like the strategy game [0 A.D.](https://software.manjaro.org/package/0ad).

For people who want to play old games or games from different platforms or operating systems, the latest versions of emulators or runtimes like [DOSbox](https://software.manjaro.org/package/dosbox), [Dolphin](https://software.manjaro.org/package/dolphin-emu), [WINE](https://software.manjaro.org/package/wine), [Winetricks](https://software.manjaro.org/package/winetricks) or [VKD3D](https://software.manjaro.org/package/vkd3d) are also available in the package repositories and can be installed with just a few clicks.
