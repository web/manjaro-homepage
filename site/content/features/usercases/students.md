{
  "date": "2016-09-05T15:08:06+02:00",
  "image": "beginners-alt2.jpg",
  "screenshot": "students-alt-full.jpg",
  "icon" : "students-icon.svg",
  "name": "Teachers/Students",
  "quote": "Manjaro fits perfectly my computer science student's needs and fresh linux desires. Moreover, accomplishes all the tasks with superb stability and lightness, as my old machine deserves. Kudos to the whole team!",
  "quoteauthor": "lennart",
  "shortdescription": "Students and teachers need software that helps them with learning or teaching about science, art and other subjects. All things also need to be organized, so strict schedules are not missed and the next exam is not forgotten. Manjaro brings everything that will make teachers and students happy.",
  "title": "Teachers and Students",
  "type": "usercase-post",
  "software" : [
  
  ],
  "softwarepresented" : [
  {"name" : "Gcompris", "url" : "https://software.manjaro.org/package/gcompris-qt", "usage" : "Learning software" },
  {"name" : "Geogebra", "url" : "https://software.manjaro.org/package/geogebra", "usage" : "Geometry/mathematics" },
  {"name" : "Minuet", "url" : "https://software.manjaro.org/package/minuet", "usage" : "Teach/learn music" }
  ],
  "weight" : 0
}

Manjaro's repository of free and quality software is filled with programs for each subject. For mathematics, simple to advanced calculations can be done using scientific calculators like [Qalculate!](https://software.manjaro.org/package/qalculate-gtk) or [SpeedCrunch](https://software.manjaro.org/package/speedcrunch) that don't just copy the experience of most hardware-calculators, but allow free editing of functions and terms.

For more complex mathematics, [Cantor](https://software.manjaro.org/package/cantor) Teachers love [Geogebra](https://software.manjaro.org/package/geogebra) to show physics, trigonometry, vector operations and more for its easy-to-use graphical editing of mathematical relations, plotting of functions and setting constraints.

Physics including molecular dynamics, gravitation and coloumb forces and more can be taught using [Step](https://www.kde.org/applications/education/step/). The earth can be explored with [Marble](https://software.manjaro.org/package/marble), while the sky and stars can be viewed with [Stellarium](https://software.manjaro.org/package/stellarium).

[Minuet](https://software.manjaro.org/package/minuet)) brings a software for music education that teaches students ear training, rhythm, harmony and more and provides features for teachers to create individual lessons.

Learning is also important for the youngest. [Gcompris](https://software.manjaro.org/package/gcompris-qt) allows them to play and learn basics of a computer, elementary mathematics, geography, reading and many other things. Included are also games like puzzles, chess, memory and more that will make learning a pleasant and funny experience.

Organizing things is important, too. The Manjaro repository contains software like [Thunderbird](https://software.manjaro.org/package/thunderbird) that provides an E-Mail client, a calendar and a news-reader.

To write homework or to create presentations, [LibreOffice](https://software.manjaro.org/package/libreoffice-still) comes preinstalled.
