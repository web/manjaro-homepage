+++
title = "Branch Compare"
thumbnail = "/img/features/repositories.png"
class = "branch"
type = "branch-compare"
meta_description = "package"
meta_keywords = "packages, compare manjaro branch"

[urls]
    json = "/branch-compare/datas.x86_64.txt"
[classes]
    diff = "badger"
[colors]
    testing = "#ff9800"
    unstable = "#ef5350"
+++



