+++
class= "partners"
type = "partners"
image = "/img/partners/minisforum.png"
href = "https://www.minisforum.com/"
+++

MinisFroum was born among a group of computer engineers who are passionate about advanced technology and design. Founded in 2012, MinisForum aims to be the best Mini PC innovator, productor and supplier. With these products, customers can experience unparalleled satisfaction and reliability, and also feel surprised and excited with our small but powerful mini pc. Our partnership with Manjaro is our first step supporting the open source community and we are excited to make more Manjaro Linux pre-installed mini PCs!
