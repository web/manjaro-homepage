+++
class= "partners"
type = "partners"
image = "/img/partners/manjaro-computer.png"
href = "https://manjarocomputer.eu/"
+++

LINUX-SERVICE.BE BVBA is a small computer reseller from Belgium offering quality notebooks with European software, assembled in the Netherlands and software installed in Belgium. With their great aftersales service LINUX-SERVICE.BE BVBA helps their customers to keep their private data were they belong and their life save from getting exposed to big corporations. Working closely together with Manjaro to bring evolution into computing by giving the Linux world a new face and operating experience. 
