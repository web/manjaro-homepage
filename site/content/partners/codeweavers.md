+++
class= "partners"
type = "partners"
image = "/img/partners/codeweavers.jpg"
href = "https://www.codeweavers.com/"
+++

We're rebels. We're misfits. But mostly, we're software liberators. And we're very, very good at what we do. We have to be. Lots of developers work with open source, but only a tiny fraction of those are good enough to get software that was designed for one platform to work on another one. We invented CrossOver software - a completely unique approach to cross-platform compatibility that does not require dual-boot or another OS license. We launched PortJump to help app and game developers broaden their market beyond Windows® users. And we launched ExecMode to help Enterprise organizations solve really ugly technical challenges.
